
import contactos from './contactos';

const expect = global.expect;

describe('contactos', () => {

	const variosContactos = [{
		nombre: 'John',
		apellido: 'Villegas',
		email: 'john@gmail.com',
		id: 1,
	}, {
		nombre: 'Jane',
		apellido: 'Villegas',
		email: 'jane@gmail.com',
		id: 2,
	}, {
		nombre: 'Yolo',
		apellido: 'Villegas',
		email: 'yolo@gmail.com',
		id: 3,
	}];

	describe('incluir', () => {
		beforeEach(() => {
			contactos.reiniciar();
		});

		test('Debe agregar un contacto con los siguientes campos {nombre, apellido, email, id}', () => {
			const contacto = variosContactos[0];

			contactos.incluir(contacto);

			const actual = contactos.db();
			const esperado = [contacto];
			expect(actual).toEqual(esperado);
		});

		test('Debe mostrar un error si no contiene los siguientes campos {nombre,apellido, email}', () => {
			const contacto = {
				nombre: 'Steven',
				apellido: 'Villegas',
				email: 's@gmail.com'

			};

			expect(() => contactos.incluir(contacto)).toThrow('Formato inválido');
		});

	});

	describe('borrar', () => {
		beforeEach(() => {
			contactos.reiniciar();
			variosContactos.forEach(contacto => contactos.incluir(contacto));
		});

		test('Debe borrar solo el primer contacto', () => {
			contactos.borrar(1);

			const actual = contactos.db();
			const esperado = [
				variosContactos[1],
				variosContactos[2],
				variosContactos[3],
			];

			expect(actual).toEqual(esperado);
		});

		test('No debe borrar ningún contacto si el id no se encontró', () => {
			contactos.borrar(100);
			const actual = contactos.db();
			const esperado = variosContactos;

			expect(actual).toEqual(esperado);
		});

	});

	
});
